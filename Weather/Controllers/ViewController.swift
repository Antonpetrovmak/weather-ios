//
//  ViewController.swift
//  Weather
//
//  Created by Petrov Anton on 3/6/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var allInfoLabel: UILabel!
    
    private var cities = [WTCity]()
    private var selectedCity : WTCity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "WTCityTVCell", bundle: nil), forCellReuseIdentifier: "WTCityTVCell")
        
        searchBar.delegate = self
        
        cityView.alpha = 0
        cityView.layer.cornerRadius = 5
        cityView.layer.borderWidth = 1
        cityView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Observer Methods
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
        if let userInfo = notification.userInfo, let keyboardSize = userInfo[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
            self.heightTableView.constant = self.view.bounds.maxY - searchBar.frame.maxY - keyboardSize.height
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardWillDisappear(_ notification: NSNotification) {
        self.heightTableView.constant = 0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - Handlers
    
    private func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 100000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func setSelectedCity(city: WTCity) {
        cityView.alpha = 1
        cityNameLabel.text = city.prettyNameOfCity()
        descriptionLabel.text = city.prettyDescription()
        allInfoLabel.text = city.prettyTemp() + ", " + city.prettyWind() + ", " + city.prettyHumidity() + ", " + city.prettyPressure() + ", " + city.prettyGeo()
        
        if let url = city.weather.first?.iconURL() {
            icon.sd_setImage(with: url)
        }
        
        if city.latitude != nil && city.longitude != nil {
            let initialLocation = CLLocation(latitude: CLLocationDegrees(city.latitude!) , longitude: CLLocationDegrees(city.longitude!))
            self.centerMapOnLocation(location: initialLocation)
        }
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.cities.count > 0 {
            return cities.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.cities.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WTCityTVCell", for: indexPath) as! WTCityTVCell
            let city = self.cities[indexPath.row]
            cell.setCityInCell(cityName: city.prettyNameOfCity(),
                               desc: city.prettyDescription(),
                               geo: city.prettyGeo(),
                               _url: city.weather.first?.iconURL())
            return cell
        }
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "MTDefaultCell")
        cell.textLabel?.text = "No results"
        cell.textLabel?.textColor = UIColor.lightGray
        cell.textLabel?.textAlignment = .center
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.cities.count > 0 {
            self.searchBar.resignFirstResponder()
            self.setSelectedCity(city: self.cities[indexPath.row])
        }
    }
}

extension ViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)  {
        
        WTAPIManager.default.listOfCities(city: searchBar.text ?? "") { [weak self](responce, error) in
            guard let strongSelf = self else { return }
            if let dict = responce as? [String: Any], let list = dict["list"] as? [[String: Any]] {
                var newCities = [WTCity]()
                for item in list {
                    guard let city = WTCity(JSON: item) else { continue }
                    newCities.append(city)
                }
                strongSelf.cities = newCities
            } else {
                strongSelf.cities = []
            }
            strongSelf.tableView.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}

