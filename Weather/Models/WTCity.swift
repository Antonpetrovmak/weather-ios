//
//  WTCity.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import ObjectMapper

class WTCity: Mappable {
    
    var id : Int?
    var name : String?
    
    var longitude : CGFloat?    // City geo location, longitude
    var latitude : CGFloat?     // City geo location, latitude
    
    var base : String?          // Internal parameter
    var clouds : Int?           // Cloudiness, %
    var rain: CGFloat?          // Rain volume for the last 3 hours
    var snow : CGFloat?         // Snow volume for the last 3 hours
    var dt : Date?              // Time of data calculation, unix, UTC
    
    var weather : [WTWeather] = []
    var main : WTMain?
    var wind : WTWind?
    var sys : WTSystem?
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        longitude   <- map["coord.lon"]
        latitude    <- map["coord.lat"]
        base        <- map["base"]
        clouds      <- map["clouds.all"]
        rain        <- map["rain.3h"]
        snow        <- map["snow.3h"]
        
        weather     <- map["weather"]
        main        <- map["main"]
        wind        <- map["wind"]
        sys         <- map["sys"]
        dt          <- (map["dt"], DateTransform())
    }
}

extension WTCity {
    func prettyDescription() -> String {
        let temp = main?.temp != nil ? String(format: "%.1f", main!.temp!) : "_"
        let desc = weather.first?.description ?? "_"
        return "Temp: \(temp) °С, (\(desc))"
    }
    
    func prettyNameOfCity() -> String {
        return "\(name ?? "_") , \(sys?.country ?? "_")"
    }
    
    func prettyGeo() -> String {
        return "Geo coords: [\(longitude != nil ? String(format: "%.4f", longitude!) : "_") , \(latitude != nil ? String(format: "%.4f", latitude!) : "_")]"
    }
    
    func prettyTemp() -> String {
        return "Temperature from \(main?.tempMin != nil ? String(format: "%.1f", main!.tempMin!) : "_") to \(main?.tempMax != nil ? String(format: "%.1f", main!.tempMax!) : "_") °С"
    }
    
    func prettyWind() -> String {
        return "Wind \(wind?.speed != nil ? String(format: "%.1f", wind!.speed!) : "_") m/s, degree \(wind?.deg != nil ? String(format: "%.0f", wind!.deg!) : "_")°"
    }
    
    func prettyHumidity() -> String {
        return "Humidity \( main?.humidity != nil ? String(format: "%.0f", main!.humidity!) : "_") % "
    }
    
    func prettyPressure() -> String {
        return "Pressure \( main?.pressure != nil ? String(format: "%.0f", main!.pressure!) : "_") hpa "
    }
}
