//
//  WTSystem.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import ObjectMapper

class WTSystem: Mappable {

    var id : Int?           // Internal parameter
    var type : Int?         // Internal parameter
    var message : CGFloat?  // Internal parameter
    var country : String?   // Country code (GB, JP etc.)
    var sunrise : Int?      // Sunrise time, unix, UTC
    var sunset : Int?       // Sunset time, unix, UTC

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        type    <- map["type"]
        message <- map["message"]
        country <- map["country"]
        sunrise <- map["sunrise"]
        sunset  <- map["sunset"]
    }
}
