//
//  WTMain.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import ObjectMapper

class WTMain: Mappable {

    
    var temp : CGFloat?         // Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var pressure : CGFloat?     // Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
    var humidity : CGFloat?     // Humidity, %
    var tempMin : CGFloat?      // Minimum temperature at the moment.
    var tempMax : CGFloat?      // Maximum temperature at the moment.
    var seaLevel : CGFloat?     // Atmospheric pressure on the sea level, hPa
    var grndLevel : CGFloat?    // Atmospheric pressure on the ground level, hPa

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        temp        <- map["temp"]
        pressure    <- map["pressure"]
        humidity    <- map["humidity"]
        tempMin     <- map["temp_min"]
        tempMax     <- map["temp_max"]
        seaLevel    <- map["sea_level"]
        grndLevel   <- map["grnd_level"]
    }
}
