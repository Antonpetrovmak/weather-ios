//
//  WTWeather.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import ObjectMapper

class WTWeather: Mappable {
    
    var id : Int?               // Weather condition id
    var main : String?          // Group of weather parameters (Rain, Snow, Extreme etc.)
    var description : String?   // Weather condition within the group
    var icon : String?          // Weather icon id


    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        main        <- map["main"]
        description <- map["description"]
        icon        <- map["icon"]
    }
}

extension WTWeather {
    func iconURL() -> URL? {
        guard let name = icon else { return nil }
        return URL.init(string: "http://openweathermap.org/img/w/\(name).png")
    }
}
