//
//  WTWind.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import ObjectMapper

class WTWind: Mappable {
    
    var speed : CGFloat?    // Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
    var deg : CGFloat?      // Wind direction, degrees (meteorological)
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        speed   <- map["speed"]
        deg     <- map["deg"]
    }
}
