//
//  WTCityTVCell.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import SDWebImage

class WTCityTVCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var geoLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setCityInCell(cityName: String, desc: String, geo: String, _url: URL?) {
       
        self.cityNameLabel.text = cityName
        self.tempLabel.text = desc
        self.geoLabel.text = geo
        
        if let url = _url {
            iconImageView.sd_setImage(with: url, completed: nil)
        } else {
            iconImageView.image = nil
        }
    }
    
}
