//
//  WTAPIManager.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit
import Alamofire

enum WTServerError: Error {
    case unknownError
    case parsingError
    case unauthorized
    case serverError(message: String?, code: Int?)
}

class WTAPIManager: NSObject {
    
    // MARK: - Public
    
    static let `default` = WTAPIManager()
    
    private override init() {
        reachabilityManager.listener = { (status) in
        }
        reachabilityManager.startListening()
    }
    
    // MARK: - Private
    
    private static let kServerScheme: String = "https"
    private static let kServerDomain: String = "api.openweathermap.org/data/2.5/"
    private static let kAPIKey: String = "5d7f7a62a2168f7c353ea2902666cc77"
    private static let kServerUrl: String = "\(kServerScheme)://\(kServerDomain)"
    
    private let reachabilityManager = Alamofire.NetworkReachabilityManager(host: kServerDomain)!
    
    private func requestURL(_ stringURL: String, method: HTTPMethod, parameters: Parameters?, success: @escaping (Any) -> Void, failure: @escaping (Error) -> Void) {
        
        let fullURL: String = WTAPIManager.kServerUrl.appending(stringURL)
        
        let headers = ["Content-Type" : "application/json",
                       "Accept" : "application/json",
                       "x-api-key": WTAPIManager.kAPIKey]
        
        print("ℹ️ [WTAPIManager] Request: \(fullURL)")
        if let parametersInfo = parameters as NSDictionary? {
            print("ℹ️ Parameters: \(parametersInfo)")
        }
        
        Alamofire.request(fullURL, method: method, parameters: parameters, encoding: method == .get ? URLEncoding.default : JSONEncoding.default, headers: headers).responseJSON { (response) in
            self.handleResponse(response, success: success, failure: failure)
        }
        
    }
    
    private func handleResponse(_ response: DataResponse<Any>, success: @escaping (Any) -> Void, failure: @escaping (Error) -> Void) {
        let responseHttpCode = response.response?.statusCode ?? 0
        let url = response.request?.url?.absoluteString
        print("ℹ️ [WTAPIManager] Response: \(url ?? "") Status: \(responseHttpCode)")
        
        switch response.result {
        case .success(let value):
            
            switch responseHttpCode {
            case 400...:
                print("❌ Error: \(value)")
                guard let response = value as? [String : Any] else {
                    failure(WTServerError.parsingError)
                    return
                }
                
                if let message = response["message"] as? String {
                    let error = WTServerError.serverError(message: message, code: responseHttpCode)
                    failure(error)
                }
                
            default:
                print("✅ Success: \(value)")
                success(value)
            }
            
        case .failure(let error):
            print("❌ Error: \(error.localizedDescription)")
            failure(error)
        }
    }
    
    // MARK: - API Methods
    
    func listOfCities(city: String, _ completion: ((Any?, Error?) -> Void)? = nil) {
        let paramrters : Parameters = ["q": city,
                                       "type": "like",
                                       "mode": "json",
                                       "units": "metric"]
        requestURL("find", method: .get, parameters: paramrters, success: { (responce) in
            completion?(responce, nil)
        }) { (error) in
            completion?(nil, error)
        }
    }

}
