//
//  UIViewController.swift
//  Weather
//
//  Created by Petrov Anton on 3/7/18.
//  Copyright © 2018 apm. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showError(_ error: Error?) {
        var errorMessage = ""
        if let error = error as? WTServerError {
            switch error {
            case .serverError(let message, _):
                
                if let message = message {
                    errorMessage.append(message + "\n")
                }
                
            case .unauthorized:
                errorMessage = "Unauthorized"
            case .parsingError:
                errorMessage = "Response parsing error"
            default:
                errorMessage = "Unknown server error"
            }
        } else {
            errorMessage = error?.localizedDescription ?? "Unknown server error"
        }
        
        let alert = UIAlertController(title: "Error", message: errorMessage.trimmingCharacters(in: .whitespacesAndNewlines), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }
}
